/**
 * TextParser
 * A simple text parsing library that accepts a File and parses the file for
 * words, sentences, and paragraphs. All words are captured in a HashMap
 * and can be queried for existence in the document. All word frequencies are
 * also captured. Library requires that parse() be called before any other method
 * call.
 *
 * @author Sean Marpo / Brandon Livitski
 * @version 9/29/15
 */

import java.io.*;
import java.util.*;
import java.util.regex.*;

public class TextParser {

   private File beingParsed;
   private Map<String, Integer> frequencyMap;
   private int sentenceCnt, wordCnt, paragraphCnt;

   public TextParser(File file) throws IOException {
      beingParsed = file;
      sentenceCnt = wordCnt = paragraphCnt = 0;
      frequencyMap = new HashMap<String, Integer>();
   }

   public void parse() throws IOException {
      BufferedReader reader = new BufferedReader(new FileReader(beingParsed));
      String curLine = reader.readLine();
      boolean textStarted = false;
      String newLinePattern = "[\r\n\t]";

      while (curLine != null) {
         // Covers the multiple new lines at the start of a file case
         if ((curLine.matches(newLinePattern) || curLine.isEmpty()) && textStarted) {
            paragraphCnt++;
            curLine = reader.readLine();
            textStarted = false;
            continue;
         }
         // Covers the multiple new lines after a paragraph
         // and multiple new lines at end of file case
         else if (curLine.matches(newLinePattern) || curLine.isEmpty()) {
            curLine = reader.readLine();
            continue;
         }

         // Fixing end of sentences without proper space after punctuation
         curLine = fixSentences(curLine);

         // Parse the current line
         parseLine(curLine);
         // Update that words were read, grab the next line of the file
         textStarted = true;
         curLine = reader.readLine();
      }

      // Parse finished! Print stats!
      printStats();
   }

   public void reportFoundWordsAndFrequencies() {
      for (String s : frequencyMap.keySet()) {
         System.out.println("Word: " + s
            + " | Frequency: "+ frequencyMap.get(s));
      }
   }

   public boolean wordExists(String word) {
      if (frequencyMap.get(word) != null) {
         return true;
      }
      else {
         return false;
      }
   }

   public void reportExactFrequency(int value) {
      for (String s : frequencyMap.keySet()) {
         if (frequencyMap.get(s) == value) {
            System.out.println(s + " - frequency: " + value);
         }
      }
   }

   public void reportExceedingFrequency(int value) {
      for (String s : frequencyMap.keySet()) {
         if (frequencyMap.get(s) > value) {
            System.out.println(s + " - frequency: " + frequencyMap.get(s));
         }
      }
   }

   /**
    * Private Helper Methods
    */

   private void printStats() {
      // Fix paragraph count (last paragraph isn't counted)
      if (paragraphCnt > 0) {
         paragraphCnt++;
      }

      // Print statistics from parse
      System.out.println("Sentences: " + sentenceCnt);
      System.out.println("Paragraphs: " + paragraphCnt);
      System.out.println("Unique Words: " + frequencyMap.size());
      System.out.println("Words: " + wordCnt);
   }

   private void parseLine(String line) {
      Scanner scanner = new Scanner(line);

      while (scanner.hasNext()) {
         String chunk = scanner.next();

         countSentences(chunk);

         // Clean the string of special characters
         // Take it to lower case
         String cleaned = removeSymbols(chunk);

         if (!cleaned.isEmpty()) {
            if (frequencyMap.containsKey(cleaned)) {
               frequencyMap.put(cleaned, frequencyMap.get(cleaned) + 1);
            }
            else {
               frequencyMap.put(cleaned, 1);
            }
            wordCnt++;
         }
      }
   }

   private void countSentences(String chunk) {
      // Obvious line endings, add a sentence
      if (chunk.contains("!") || chunk.contains("?") || chunk.contains("....")) {
         sentenceCnt++;
      }
      // Handle the period case because ... breaks normal checking
      else if (chunk.contains(".") && !chunk.contains("...")) {
         sentenceCnt++;
      }
   }

   private String removeSymbols(String chunk) {
      // Remove leading and trailing single quotes (ticks)
      if (chunk.charAt(0) == '\'') {
         chunk = chunk.substring(1);
      }
      else if (chunk.charAt(chunk.length() - 1) == '\'') {
         chunk = chunk.substring(0, chunk.length() - 2);
      }
      // Remove single dashes read in as words
      if (chunk.length() == 1 && chunk.charAt(0) == '-') {
         chunk = "";
      }

      return chunk.replace("!", "").replace("?", "").replace(".", "").replace(",", "").replace("\"", "").replace(":", "").replace("(", "").replace(")", "").replace(";", "").toLowerCase();
   }

   private String fixSentences(String line) {
      // Add a space after every sentence ending character
      line = line.replaceAll("\\.|!|\\?([ -~])", ". $1");
      // Replace all double dashes with single so they are read correctly
      line = line.replace("--", " - ");
      return line;
   }
}
