/**
 * CSVParser
 * -- Description of CSVParser here --
 *
 * @author Sean Marpo / Brandon Livitski
 * @version 9/29/15
 */

import java.io.*;
import java.util.*;

public class CSVParser {

	private List<String> vectorList;

	public CSVParser(File inputFile) throws IOException {
		Scanner fileScanner = new Scanner(inputFile);
		int i;
		String tempVectorString;

		vectorList = new ArrayList<String>();
		while (fileScanner.hasNextLine()) {
			vectorList.add(fileScanner.nextLine());
		}

		for (i = 0; i < vectorList.size(); i++) {
			tempVectorString = vectorList.get(i);
			if (tempVectorString.charAt(0) == ',') {
				tempVectorString = "0" + tempVectorString;
			}
			tempVectorString = tempVectorString.replace(",,", ",0,");
			vectorList.set(i, tempVectorString);
		}
	}

	public double computeLength(int vectorNumber) {
		double sumOfSquares = 0;
		Scanner lineScanner = new Scanner(vectorList.get(vectorNumber));
		double currentValue = 0;

		lineScanner.useDelimiter(",");
		while (lineScanner.hasNext()) {
			currentValue = Double.parseDouble(lineScanner.next());
			sumOfSquares += currentValue * currentValue;
		}

		// System.out.println(Math.sqrt(sumOfSquares));
		return Math.sqrt(sumOfSquares);
	}

	public double computeDotProduct(int vector1, int vector2) {
		Scanner vector1Parser = new Scanner(vectorList.get(vector1));
		vector1Parser.useDelimiter(",");
		Scanner vector2Parser = new Scanner(vectorList.get(vector2));
		vector2Parser.useDelimiter(",");

		double dotProductSum = 0;

		while (vector1Parser.hasNext() && vector2Parser.hasNext()) {
			dotProductSum += (Double.parseDouble(vector1Parser.next())) * (Double.parseDouble(vector2Parser.next()));
		}
		return dotProductSum;
	}

	public double computeEucledianDistance(int vector1, int vector2) {
		Scanner vector1Parser = new Scanner(vectorList.get(vector1));
		vector1Parser.useDelimiter(",");
		Scanner vector2Parser = new Scanner(vectorList.get(vector2));
		vector2Parser.useDelimiter(",");

		double sumOfSquares = 0;
		double vector1temp;
		double vector2temp;

		while (vector1Parser.hasNext() && vector2Parser.hasNext()) {
			vector1temp = Double.parseDouble(vector1Parser.next());
			vector2temp = Double.parseDouble(vector2Parser.next());
			sumOfSquares += (vector1temp - vector2temp) * (vector1temp - vector2temp);
		}
		return Math.sqrt(sumOfSquares);
	}

	public double computeManhattanDistance(int vector1, int vector2) {
		// String v1 = vectorList.get(vector1);
		// String v2 = vectorList.get(vector2);
		int vector1length = 0;
		int vector2length = 0;
		Scanner vector1Parser = new Scanner(vectorList.get(vector1));
		vector1Parser.useDelimiter(",");
		Scanner vector2Parser = new Scanner(vectorList.get(vector2));
		vector2Parser.useDelimiter(",");
		while (vector1Parser.hasNext()) {
			vector1Parser.next();
			vector1length++;
		}
		while (vector2Parser.hasNext()) {
			vector2Parser.next();
			vector2length++;
		}
		vector1Parser = new Scanner(vectorList.get(vector1));
		vector1Parser.useDelimiter(",");
		vector2Parser = new Scanner(vectorList.get(vector2));
		vector2Parser.useDelimiter(",");
		int maxLength = Math.max(vector1length, vector2length);
		double tempMD = 0;
		int i = 0;
		for (i = 0; i < maxLength; i++) {
			if (vector1Parser.hasNext() && vector2Parser.hasNext()) {
				tempMD += Math.abs(Double.parseDouble(vector1Parser.next()) - Double.parseDouble(vector2Parser.next()));
			} else if (vector1Parser.hasNext()) {
				tempMD += Math.abs(Double.parseDouble(vector1Parser.next()));
			} else {
				tempMD += Math.abs(Double.parseDouble(vector2Parser.next()));
			}
		}
		return tempMD;
	}

	public double computePearsonCorrelation(int vector1, int vector2) {
		double vector1SD = computeVectorStandardDeviation(vector1);
		double vector2SD = computeVectorStandardDeviation(vector2);

		Scanner vector1Parser = new Scanner(vectorList.get(vector1));
		vector1Parser.useDelimiter(",");
		Scanner vector2Parser = new Scanner(vectorList.get(vector2));
		vector2Parser.useDelimiter(",");
		int vector1length = 0;
		int vector2length = 0;

		while (vector1Parser.hasNext()) {
			Double.parseDouble(vector1Parser.next());
			vector1length++;
		}

		while (vector2Parser.hasNext()) {
			Double.parseDouble(vector2Parser.next());
			vector2length++;
		}

		if (vector1length != vector2length) {
			throw new UnsupportedOperationException();
		}

		return vector1SD * vector2SD / vector1length;
	}

	public double computeLargest(int vectorNumber) {
		List<Double> valueList = new ArrayList<Double>();
		Scanner lineScanner = new Scanner(vectorList.get(vectorNumber));
		lineScanner.useDelimiter(",");
		while (lineScanner.hasNext()) {
			valueList.add(Double.parseDouble(lineScanner.next()));
		}
		return Collections.max(valueList);
	}

	public double computeSmallest(int vectorNumber) {
		List<Double> valueList = new ArrayList<Double>();
		Scanner lineScanner = new Scanner(vectorList.get(vectorNumber));
		lineScanner.useDelimiter(",");
		while (lineScanner.hasNext()) {
			valueList.add(Double.parseDouble(lineScanner.next()));
		}
		return Collections.min(valueList);
	}

	public double computeMean(int vectorNumber) {
		Scanner lineScanner = new Scanner(vectorList.get(vectorNumber));
		lineScanner.useDelimiter(",");
		double currentSum = 0;
		double numValues = 0;
		while (lineScanner.hasNext()) {
			currentSum += Double.parseDouble(lineScanner.next());
			numValues++;
		}
		return currentSum / numValues;
	}

	public double computeMedian(int vectorNumber) {
		Scanner lineScanner = new Scanner(vectorList.get(vectorNumber));
		lineScanner.useDelimiter(",");
		int numValues = 0;

		List<Double> valueList = new ArrayList<Double>();

		while (lineScanner.hasNext()) {
			valueList.add(Double.parseDouble(lineScanner.next()));
			numValues++;
		}

		Collections.sort(valueList);

		if (numValues % 2 == 0) {
			return (valueList.get((numValues / 2) - 1) + valueList.get(numValues / 2)) / 2;
		} else {
			return valueList.get(numValues / 2);
		}
	}

	public double computeVectorStandardDeviation(int vectorNumber) {
		double vectorMean = computeMean(vectorNumber);
		double sumOfSquares = 0;

		List<Double> valueList = new ArrayList<Double>();
		Scanner lineScanner = new Scanner(vectorList.get(vectorNumber));
		lineScanner.useDelimiter(",");
		while (lineScanner.hasNext()) {
			valueList.add(Double.parseDouble(lineScanner.next()));
		}

		for (Double currentValue: valueList) {
			sumOfSquares += (vectorMean - currentValue) * (vectorMean - currentValue);
		}

		return Math.sqrt(sumOfSquares);
	}

	public double computeColumnLargest(int columnNumber) {
		List<Double> valueList = new ArrayList<Double>();
		Scanner lineScanner;
		int i;
		for (String currentVector: vectorList) {
			lineScanner = new Scanner(currentVector);
			lineScanner.useDelimiter(",");
			for (i = 0; i < columnNumber; i++) {
				lineScanner.next();
			}
			valueList.add(Double.parseDouble(lineScanner.next()));
		}
		return Collections.max(valueList);
	}

	public double computeColumnSmallest(int columnNumber) {
		List<Double> valueList = new ArrayList<Double>();
		Scanner lineScanner;
		int i;
		for (String currentVector: vectorList) {
			lineScanner = new Scanner(currentVector);
			lineScanner.useDelimiter(",");
			for (i = 0; i < columnNumber; i++) {
				lineScanner.next();
			}
			valueList.add(Double.parseDouble(lineScanner.next()));
		}
		return Collections.min(valueList);
	}

	public double computeColumnMean(int columnNumber) {
		List<Double> valueList = new ArrayList<Double>();
		Scanner lineScanner;
		double currentSum = 0;
		int i;
		for (String currentVector: vectorList) {
			lineScanner = new Scanner(currentVector);
			lineScanner.useDelimiter(",");
			for (i = 0; i < columnNumber; i++) {
				lineScanner.next();
			}
			currentSum += Double.parseDouble(lineScanner.next());
		}
		return currentSum / vectorList.size();
	}

	public double computeColumnMedian(int columnNumber) {
		Scanner lineScanner;
		List<Double> valueList = new ArrayList<Double>();
		int i;

		for (String currentVector: vectorList) {
			lineScanner = new Scanner(currentVector);
			lineScanner.useDelimiter(",");

			for (i = 0; i < columnNumber; i++) {
				lineScanner.next();
			}
			valueList.add(Double.parseDouble(lineScanner.next()));
		}

		Collections.sort(valueList);

		if (vectorList.size() % 2 == 0) {
			return (valueList.get((vectorList.size() / 2) - 1) + valueList.get(vectorList.size() / 2)) / 2;
		} else {
			return valueList.get(vectorList.size() / 2);
		}
	}

	public double computeColumnStandardDeviation(int columnNumber) {
		double columnMean = computeColumnMean(columnNumber);
		double sumOfSquares = 0;
		int i;
		Scanner lineScanner;

		List<Double> valueList = new ArrayList<Double>();

		for (String currentVector: vectorList) {
			lineScanner = new Scanner(currentVector);
			lineScanner.useDelimiter(",");
			for (i = 0; i < columnNumber; i++) {
				lineScanner.next();
			}
			valueList.add(Double.parseDouble(lineScanner.next()));
		}

		for (Double currentValue: valueList) {
			sumOfSquares += (columnMean - currentValue) * (columnMean - currentValue);
		}

		return Math.sqrt(sumOfSquares);
	}

	public List<String> getVectorList() {
		return vectorList;
	}
}
