/**
 * Main
 * Sample Driver program that will instantiate a CSVParser and TextParser.
 * You can supply as many file arguments as needed just ensure the test files end in
 * .csv or .txt otherwise the program will produce an error.
 *
 * @author Sean Marpo / Brandon Livitski
 * @version 9/29/15
 */

import java.io.*;
import java.util.*;

public class Main {

   public static void main(String[] args) throws IOException {
      for (String s : args) {
         File file = new File(s);

         System.out.println("File Name: " + s);

         /* Create a CSVParser and run all options */
         if (s.contains(".csv")) {
            CSVParser csv = new CSVParser(file);
            List<String> vectorList = csv.getVectorList();
            String vector1 = vectorList.get(0);
            String vector2 = vectorList.get(1);
            System.out.println("Computing length of:\n" + vector1);
            System.out.println(csv.computeLength(0) + "\n");
            System.out.println("Computing dot product of:\n" + vector1 + "\n" + vector2);
            System.out.println(csv.computeDotProduct(0, 1) + "\n");
            System.out.println("Computing Eucledian distance between:\n" + vector1 + "\n" + vector2);
            System.out.println(csv.computeEucledianDistance(0, 1) + "\n");
            System.out.println("Computing Manhattan distance between:\n" + vector1 + "\n" + vector2);
            System.out.println(csv.computeManhattanDistance(0, 1) + "\n");
            System.out.println("Computing Pearson Correlation for:\n" + vector1 + "\n" + vector2);
            try {
               double tempDouble = csv.computePearsonCorrelation(0, 1);
               System.out.println(tempDouble + "\n");
            } catch (Exception e) {
               System.out.println("Please use vectors with the same number of dimensions." + "\n");
            }
            System.out.println("Computing largest in vector:\n" + vector1);
            System.out.println(csv.computeLargest(0) + "\n");
            System.out.println("Computing smallest in vector:\n" + vector1);
            System.out.println(csv.computeSmallest(0) + "\n");
            System.out.println("Computing mean of vector:\n" + vector1);
            System.out.println(csv.computeMean(0) + "\n");
            System.out.println("Computing median of vector:\n" + vector1);
            System.out.println(csv.computeMedian(0) + "\n");
            System.out.println("Computing standard deviation of vector:\n" + vector1);
            System.out.println(csv.computeVectorStandardDeviation(0) + "\n");
            System.out.println("Computing largest in row: " + 0);
            System.out.println(csv.computeColumnLargest(0) + "\n");
            System.out.println("Computing smallest in row: " + 0);
            System.out.println(csv.computeColumnSmallest(0) + "\n");
            System.out.println("Computing mean of row: " + 0);
            System.out.println(csv.computeColumnMean(0) + "\n");
            System.out.println("Computing median of row: " + 0);
            System.out.println(csv.computeColumnMedian(0) + "\n");
            System.out.println("Computing standard deviation of row: " + 0);
            System.out.println(csv.computeColumnStandardDeviation(0));
         }
         /* Create a TextParser and run all options */
         else if (s.contains(".txt")) {
            TextParser text = new TextParser(file);
            text.parse();
            text.reportFoundWordsAndFrequencies();

            /* Checks to see if a word exists */
            System.out.println(text.wordExists("word_to_be_checked"));

            /* Reports all words with the exact frequency specified */
            text.reportExactFrequency(5);

            /* Reports all words that exceed the frequency specified */
            text.reportExceedingFrequency(10);
         }
         else {
            System.out.println("File type could not be determined. Please end file with .csv or .txt");
         }
      }
   }
}
